// 3. Create a mock database
let directory = [

	{
	     "firstName": "Mary Jane",
	     "lastName": "Dela Cruz",
	     "mobileNo": "09123456789",
	     "email": "mjdelacruz@mail.com",
	     "password": 123
	},
	{
	     "firstName": "John",
	     "lastName": "Doe",
	     "mobileNo": "09123456789",
	     "email": "jdoe@mail.com",
	     "password": 123
	}

];


// Create Server
const http = require('http');
const port = 4000

const server = http.createServer((req, res) => {

	// 4. Create a route "/profile" and request all the information in the data
	if(req.url == '/profile' && req.method == 'GET'){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory))
		res.end()
	}

	// 5. Create a route for creating a new item upon receiving a POST request
	if(req.url == '/profile' && req.method == 'POST'){

		let profileData = '';

		req.on('data', function(data){
			profileData += data
		})


		req.on('end', function(){
			profileData = JSON.parse(profileData)


			// Create new object

			let newProfile = {
				"firstName": profileData.firstName,
				"lastName": profileData.lastName,
				"mobileNo": profileData.mobileNo,
				"email": profileData.email,
				"password": profileData.password
			}

			directory.push(newProfile);
			console.log(directory);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newProfile));
			res.end()
		})

	}

})


server.listen(port);
console.log(`Server running at localhost:${port}`)